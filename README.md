# ANDROID DEVELOPMENT EXAM #

### PROJECT: SONG LYRICS APP ###
This app will show different stored song titles, and display its lyrics.

### Tasks ###
1. The app should be able to import the data (song lyrics, artists info, etc.) on the provided JSON file. This should only be done once by the app (which means that the app should not constantly load data from the JSON file every now and then).
2. The main page of the app should display a list of all songs (with a subtext displaying the artist of that song). The list should be initially presented in alphabetical order by the song title.
3. Add an option which can filter the items in the list of songs by a given artist (you can just use a dropdown picker for this, with options from the existing artists given in the JSON file)
4. If the user will tap on the song, it will redirect you to the page which display the information about the song, and the lyrics of that song. Make sure that the name of the artist should also be present there.
5. In the lyrics page, if the user will tap on the name of the artist, it will redirect to page which shows the artist's information
6. In the lyrics page, add a button which will redirect to a form which will let the user edit the lyrics of that song
7. For bonus points, make sure that the given image file named favicon.jpg will be used as the icon of the app.
